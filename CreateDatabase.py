# -*- encoding : utf-8 -*-

import sqlite3
import sys

students = {("Jean", "Chimie", "Sciences"),
            ("Pierre", "Chimie", "Sciences"),
            ("Anne", "Marketing", "Economie"),
            ("Eric", "Biologie", "Sciences")}

DF1 = {("t", "A B", "C"), ("t", "A B C", "D"),("students","Nom","Departement"),("students","departement","fac")}

myDatabase = sqlite3.connect("Databases/test.db")

with myDatabase:

    cursor = myDatabase.cursor()

    cursor.execute('''DROP TABLE IF EXISTS students''')
    cursor.execute('''DROP TABLE IF EXISTS FuncDep''')
    cursor.execute('''DROP TABLE IF EXISTS t''')
    cursor.execute('''CREATE TABLE t("A" TEXT,"B" TEXT, "C" TEXT,
                                        "D" TEXT, "E" TEXT)''')
    cursor.execute("CREATE TABLE students(Nom TEXT, Departement TEXT, Fac TEXT)")
    cursor.execute('''CREATE TABLE FuncDep("table" TEXT,"lhs" TEXT, "rhs" TEXT)''')

    cursor.executemany('''INSERT INTO FuncDep VALUES (?, ?, ?)''', DF1)

    cursor.execute('''SELECT * FROM t''')

    print "STUDENT"
    print "%10s | %10s | %13s\n" % ("Nom", "Section", "Departement")
    while True:
        row = cursor.fetchone()
        if row == None:
            break

        print "%10s | %10s | %10s" % (row[0], row[1], row[2])

    cursor.execute('''SELECT * FROM FuncDep''')

    print "\nFuncDep"
    print "%12s | %12s | %12s\n" % ("table", "lhs", "rhs")
    while True:

        row = cursor.fetchone()

        if row == None:
            break

        print "%12s | %12s | %12s" % (row[0], row[1], row[2])

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

countries = {("Belgium", "Brussels", 10255, "EUR"),
             ("Belgium", "Brussels", 105, "EUR"),
             ("Norway", "Oslo", 4463, "NOK"),
             ("Japan", "Tokyo", 128888, "YEN")}

DF2 = {("Countries", "Name Capital", "Population")}

myDatabase2 = sqlite3.connect("Databases/country.db")

with myDatabase2:

    cursor2 = myDatabase2.cursor()

    cursor2.execute('''DROP TABLE IF EXISTS resultats_cyclisme''')
    cursor2.execute('''DROP TABLE IF EXISTS Countries''')
    cursor2.execute('''DROP TABLE IF EXISTS FuncDep''')
    cursor2.execute('''CREATE TABLE Countries("Name" TEXT,"Capital" TEXT, "Population" INTEGER(2), "Currency" TEXT)''')
    cursor2.execute('''CREATE TABLE FuncDep("table" TEXT,"lhs" TEXT, "rhs" TEXT)''')
    cursor2.executemany('''INSERT INTO FuncDep VALUES (?, ?, ?)''', DF2)
    cursor2.executemany('''INSERT INTO Countries VALUES (?, ?, ?, ?)''', countries)
    cursor2.execute('''SELECT * FROM Countries''')

    print "\nCountries"
    print "%12s | %12s | %12s | %12s\n" % ("Name", "Capital", "Population","Currency")
    while True:

        row = cursor2.fetchone()

        if row == None:
            break

        print "%12s | %12s | %12i | %12s" % (row[0], row[1], row[2], row[3])

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

resultats_cyclisme = {("Tour des Flandres", "GILBERT", "BMC", 2012, "Avril", 1, 75, 11),
    ("Liege-Bastogne-Liege", "GILBERT", "BMC", 2012, "Avril", 22, 16, 1),
    ("Liege-Bastogne-Liege", "IGLINSKIY", "ASTANA", 2012, "Avril", 22, 1, 25),
    ("Liege-Bastogne-Liege", "IGLINSKIY", "OMEGA", 2011, "Avril", 24, 1, 101),
    ("Tour des Flandres", "GILBERT","OMEGA", 2010, "Avril", 4, 3, 93)}

df = {("resultats_cyclisme", "Coureur Annee", "Equipe"),
    ("resultats_cyclisme", "Course Annee Dossard", "Coureur"),
    ("resultats_cyclisme", "Course Annee Coureur","Classement"),
    ("resultats_cyclisme", "Course Annee Dossard Coureur","Classement"),
    ("resultats_cyclisme", "Course Annee", "Mois"),
    ("resultats_cyclisme", "Course Annee", "Jour"),
    ("resultats_cyclisme", "Annee Mois Jour", "Course"),
    ("pouet", "A", "B"), ("pouet", "B", "C"),
    ("pouet", "B A", "C"), ("tg", "Jean", "Onche")}

myDatabase3 = sqlite3.connect("Databases/cyclism.db")

with myDatabase3:

    cursor3 = myDatabase3.cursor()

    cursor3.execute('''DROP TABLE IF EXISTS resultats_cyclisme''')
    cursor3.execute('''DROP TABLE IF EXISTS FuncDep''')
    cursor3.execute('''CREATE TABLE resultats_cyclisme("Course" TEXT,"Coureur" TEXT, "Equipe" TEXT, '''
                    '''"Annee" INTEGER(2), "Mois" TEXT, "Jour" INTEGER(2), "Classement" INTEGER(2),
                    "Dossard" INTEGER(2))''')
    cursor3.execute('''CREATE TABLE FuncDep("table" TEXT,"lhs" TEXT, "rhs" TEXT)''')
    cursor3.executemany('''INSERT INTO FuncDep VALUES (?,?,?)''', df)
    cursor3.executemany('''INSERT INTO resultats_cyclisme VALUES (?, ?, ?, ?, ?, ?, ?, ?)''', resultats_cyclisme)
    cursor3.execute('''SELECT * FROM resultats_cyclisme''')

    print "\nresultats_cyclisme"
    print "%20s | %12s | %12s | %12s | %12s | %12s | %12s | %12s |\n" % ("Course","Coureur","Equipe",
                    "Annee", "Mois", "Jour", "Classement", "Dossard")
    while True:

        row = cursor3.fetchone()

        if row == None:
            break

        print "%20s | %12s | %12s | %12i | %12s | %12i | %12i | %12i" % (row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7])

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

df = {('t','Arabesque Bretagne Chien Detritus Enfant Film', 'Detritus'), ('t', 'Arabesque Chien', 'Enfant'), ('t','Arabesque Bretagne Detritus', 'Chien'), ('t', 'Enfant Bretagne', 'Film'), ('t', 'Enfant Film', 'Arabesque'), ('t', 'Enfant Film', 'Bretagne'), ('t', 'Enfant Film', 'Chien'), ('t', 'Arabesque Film', 'Bretagne'), ('t', 'Arabesque Film', 'Chien')}

myDatabase42 = sqlite3.connect("Databases/new.db")

with myDatabase42:
    cursor4 = myDatabase42.cursor()

    cursor4.execute('''DROP TABLE IF EXISTS t''')
    cursor4.execute('''DROP TABLE IF EXISTS FuncDep''')
    cursor4.execute('''CREATE TABLE t ("Arabesque" TEXT, "Bretagne" TEXT, "Chien" TEXT, "Detritus" TEXT,
     "Enfant" TEXT, "Film" TEXT)''')
    cursor4.execute('''CREATE TABLE FuncDep("table" TEXT,"lhs" TEXT, "rhs" TEXT)''')
    cursor4.executemany('''INSERT INTO FuncDep VALUES (?,?,?)''', df)