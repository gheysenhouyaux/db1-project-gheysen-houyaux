import sqlite3
import FunctionalDependencies

class DbObject:
    def __init__(self, db_path):
        """
        Object which initialises the connection with the database
        :param db_path: the databse we wants to connect
        :return: /
        """
        self.db_path = db_path
        self.myDatabase = sqlite3.connect(db_path)
        self.cursor = self.myDatabase.cursor()
        FunctionalDependencies.createFuncDep(self.cursor)
    def closeDb(self):
        """
        Closes the database
        :return: /
        """
        self.myDatabase.close()