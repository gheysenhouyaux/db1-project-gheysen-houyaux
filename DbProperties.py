import Keys
import Util
import FunctionalDependencies
import DbObject

def isInBCNF(myDatabase,cursor,table):
    """

    :param myDatabase: the database
    :param cursor: the cursor of the database
    :param table: the table to check
    :return: True if the table is in BCNF
    """
    for df in FunctionalDependencies.getFuncDep(myDatabase,cursor):
        if not df[1] in Keys.getSuperkeys(cursor,myDatabase,table):
            return False
    return True

def isIn3NF(myDatabase,cursor,table):
    """

    :param myDatabase: the database
    :param cursor: the cursor of the database
    :param table: the table to check
    :return: True if the table is in 3NF
    """
    for df in FunctionalDependencies.getFuncDep(myDatabase,cursor):
        if not df[1] in Keys.getSuperkeys(cursor,myDatabase,table):
            if Keys.getKeys(cursor,myDatabase,table) == []:
                return  False
            else:
                for key in Keys.getKeys(cursor,myDatabase,table):
                    if not df[2] in key:
                        return False
    return True

def tableDecomposition(myDatabase,cursor,table):
    """

    :param myDatabase: the database
    :param cursor: the cursor of the database
    :param table: the table to decompose
    :return: an array containing the decomposition of the table
    """
    if not isIn3NF(myDatabase,cursor,table):
        funcDep_prime = list()
        imgRelation = list()
        funcDep = FunctionalDependencies.getFuncDep(myDatabase,cursor)
        keys = Keys.getKeys(cursor,myDatabase,table)
        for df1 in funcDep:
            checked = False
            for df2 in funcDep:
                if df1[0] == table and df1[0] == df2[0] and df1 != df2:
                    if len(df1[1].split(" ")) == 1 and (df1[0],df1[1],df1[2]) not in funcDep_prime:
                        funcDep_prime.append((df1[0],df1[1],df1[2]))
                    elif df2[1].split(" ") in Util.getPartsOf(df1[1].split(" ")) \
                            and (df1[0],df2[1], df1[2]) not in funcDep_prime:
                        funcDep_prime.append((df1[0],df2[1], df1[2]))
                        checked = True
                    else:
                        break
            if not (df1[0],df1[1],df1[2]) in funcDep_prime and checked == False:
                funcDep_prime.append((df1[0],df1[1],df1[2]))

        for element in funcDep_prime:
            imgRelation.append((element[1].split(" ") + element[2].split(" "),
                             restrict(funcDep_prime,element[1].split(" ") + element[2].split(" "))))
        try:
            imgRelation.append((keys[0],restrict(funcDep_prime,keys[0])))
        except IndexError:
            print "Pas de cle pour cette relation, decomposition impossible"
        return imgRelation
    else:
        print "La relation "+table+" est deja en 3NF"

def restrict(funcdep,attribute):
    """

    :param myDatabase: the database
    :param cursor: the cursor of the database
    :param attribute: the attributes to find in the functional dependencies
    :return: return an array of functional dependencies where the attributes are
    """
    restrictedAttr = list()
    for df in funcdep:
        if df[1].split(" ")+df[2].split(" ") == attribute:
            restrictedAttr.append(df)
    return restrictedAttr

def decomposition(myDatabase,cursor,db_path):
    """
    decompose each table of the database
    :param myDatabase: the database
    :param cursor: the cursor of the database
    :param db_path: the path of the database
    :return:
    """
    splittedDb_path = db_path.split(".")
    splittedDb_path[0] = splittedDb_path[0]+"_3NF"
    newDb_path = ".".join(splittedDb_path)
    MaDb = DbObject.DbObject(newDb_path)
    for table in Util.getTables(cursor):
        i=1
        if table[0] != "FuncDep":
            decomposedTable = tableDecomposition(myDatabase,cursor,table[0])
            if decomposedTable != []:
                for element in decomposedTable:
                    if element[1] != []:
                        if element[1][0][0] == table[0]:
                            createTable(cursor,MaDb.cursor,table[0],table[0]+str(i),element[0],db_path)
                    else:
                        createTable(cursor,MaDb.cursor,table[0],table[0]+str(i),element[0],db_path)
                    for df in element[1]:
                        if element[1][0][0] == table[0]:
                            FunctionalDependencies.addFuncDep(MaDb.myDatabase,MaDb.cursor,table[0]+str(i),df[1],df[2])
                    i += 1

def createTable(oldCursor,newCursor,oldTable,newTable,attribute,db_path):
    """
    create a table in the new database
    :param oldCursor: the cursor of the old database
    :param newCursor: the cursor of the new database
    :param oldTable: the table of the old database
    :param newTable: the table of the new database
    :param attribute: the attribute to put in the new database
    :param db_path: the path of the old database
    :return:
    """
    newCursor.execute("ATTACH '"+db_path+"' AS oldDb")
    newCursor.execute("DROP TABLE IF EXISTS "+newTable+"")
    newCursor.execute("CREATE TABLE "+newTable+" AS SELECT "+", ".join(attribute)+" FROM oldDb."+oldTable)
    newCursor.execute("DETACH oldDb")