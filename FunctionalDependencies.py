# -*- encoding : utf-8 -*-

import sqlite3
import Util
import DbObject


def createFuncDep(cursor):
    """

    :param cursor the cursor of the database where it creates the table funcDep
    """
    found = False
    for table in Util.getTables(cursor):
        if "FuncDep" == table[0]:
            found = True
    if not found:
        cursor.execute('''CREATE TABLE FuncDep("table" TEXT,"lhs" TEXT, "rhs" TEXT)''')


def getFuncDep(myDatabase, cursor):
    """

    :param myDatabase: the dataBase where the FuncDep is
    :param cursor: the cursor of the database
    :return: an array of type [(table,lhs,rhs),...] which contains the dependencies
    """
    try:
        dependencies = []
        with myDatabase:
            cursor = myDatabase.cursor()
            cursor.execute("SELECT * FROM FuncDep")
            while True:
                row = cursor.fetchone()
                if row == None:
                    break
                dependencies.append((row[0], row[1], row[2]))
        return dependencies
    except sqlite3.OperationalError:
        print "La table des dependances n'existe pas"


def addFuncDep(myDatabase, cursor, table, lhs, rhs):
    """
    Add the specified dependence into the FuncDep table
    :param myDatabase: the dataBase where the FuncDep is
    :param cursor: the cursor of the database
    :param table: the name of the relation where the dependence is
    :param lhs: left part of the dependence with the format part1-part2
    if there is more than 1 column
    :param rhs: rigth part of the dependence
    """
    with myDatabase:
        cursor.execute('''SELECT * FROM FuncDep WHERE "table"=? AND "lhs"= ? AND "rhs"=?''', (table,lhs,rhs))
        if len(cursor.fetchall()) == 0:
            cursor.execute("INSERT INTO FuncDep VALUES (?,?,?)", (table, lhs, rhs))
            print "La dependence a bien ete ajoutee"
        else :
            print "La dependence existe deja"


def removeFuncDep(myDatabase, cursor, table, lhs, rhs):
    """
    Remove the specified dependencie from the FuncDep table
    :param myDatabase: the dataBase where the FuncDep is
    :param cursor: the cursor of the database
    :param table: the table of the dependence
    :param lhs: the left part of the dependence
    :param rhs: the right part of the dependence
    :return:
    """
    try:
        with myDatabase:
            cursor.execute('''DELETE FROM FuncDep WHERE "table"=? AND "lhs"=? AND "rhs"=?''', (table,lhs,rhs))
            print "La dependence a bien ete supprimee !"
    except sqlite3.OperationalError:
        print "Impossible de supprimer cette dependence car elle n'existe pas"


def modifFuncDep(myDatabase, cursor, old_table, old_lhs, old_rhs, table, lhs, rhs):
    """
    Modify the specified dependence in the FuncDep table with the specified values
    :param myDatabase: the dataBase where the FuncDep is
    :param cursor: the cursor of the database
    :param old_table: the table of the old dependence
    :param old_lhs: the left part of the old dependence
    :param old_rhs: the right part of the old dependence
    :param table: the table of the new dependence
    :param lhs: the left part of the new dependence
    :param rhs: the right part of the new dependence
    :return:
    """
    try:
        with myDatabase:
            cursor.execute('''UPDATE FuncDep SET "table"=?, "lhs"=?, "rhs"=?
            WHERE "table"=? AND "lhs"=? AND "rhs"=?''', (table, lhs, rhs, old_table, old_lhs, old_rhs))
    except sqlite3.OperationalError:
        print "Impossible de modifier cette dependence car elle n'existe pas"


def getDependencies(cursor, relation):
    """
    Give all the dependence in the FuncDep table
    :param cursor: the cursor of the database
    :param relation: the relation for which we display the dependencies
    :return: a array with the form : [(dep1_left_part, dep1_rigth_part),(dep2_left_part, dep2_rigth_part),...]
    with the all the dependencies of the relation
    """
    dependencies = []
    cursor.execute('''SELECT * FROM FuncDep WHERE "table" = ?''',(relation,))
    while True:
        row = cursor.fetchone()
        if row == None:
            break
        dependencies.append((row[1],row[2]))
    return dependencies


def isCorrect(cursor, df):
    """
    :param cursor: the cursor of the database
    :param df: triplet with the form (table, lhs, rhs)
    :return: True if the dependence "lhs -> rhs" is verified in the table "table"
    """
    attributes = Util.getAttributes(cursor, df[0])
    df_1 = df[1].split(" ")
    df_2 = df[2].split(" ")
    df_1_ok = True
    df_2_ok = True
    for df_1_part in df_1:
        if df_1_part not in attributes:
            df_1_ok = False
    for df_2_part in df_2:
        if df_2_part not in attributes:
            df_2_ok = False
    if df_1_ok and df_2_ok :
        tuple_length = len(df)
        cursor.execute("SELECT "+(df[1].replace(" ", ",")+","+df[2])+" FROM "+df[0]+" ")
        row = cursor.fetchall()
        for i in range(len(row)):  # Pour chaque ligne de la table, on la selectionne et compare
            for j in range(len(row)):  # On compare avec toutes les autres lignes
                lhs_ok = True  # Variable pour savoir si les valeurs de lhs sont ok entre les tuples
                for k in range(len(row[j])-1): # On check chaque element de lhs
                    if row[i][k] != row[j][k]:
                        # Si lhs est different une fois alors on ne peut pas comparer
                        lhs_ok = False
                if lhs_ok and row[i][len(row[j])-1] != row[j][len(row[j])-1]:
                    return False
                    # Si rhs est different une fois alors la df n'est pas respectee
        return True


def getIncorrectDependencies(cursor, relation):
    """
    The incorrect dependencies found in FuncDep
    :param cursor: the cursor of the database
    :param relation: the relation for which we display the incorrect dependencies
    :return: a array with the form : [(dep1_left_part, dep1_rigth_part),(dep2_left_part, dep2_rigth_part),...]
    with the all the incorrect dependencies of the relation
    """
    wrongdependencies = []
    cursor.execute('''SELECT * FROM FuncDep WHERE "table"=?''', (relation,))
    lines_table = cursor.fetchall()
    for line in lines_table:
        if not isCorrect(cursor, line):
            wrongdependencies.append((line [0], line[1],line[2]))
    return wrongdependencies


def getLogicalConsequences(cursor, relation):
    """

    :param cursor: cursor: the cursor of the database
    :param relation: the relation for which we display the logical consequences
    :return: an array which contains the logical consequences for the table "relation"
    """
    cursor.execute('''SELECT * FROM FuncDep WHERE "table" = ?''', (relation,))
    sigma = cursor.fetchall()
    lgcl_csq_array = []  # Tableau qui contiendra les consequences logiques
    for i in range(len(sigma)):  # On parcours toutes les dependances
        analysed_df = sigma[i]
        other_df = []
        other_df_lhs = []
        acquired_rhs_array = []  # Contient les attributs de gauche du tuple que l'on acquiert via une dependance
        acquired_lhs_array = []  # Idem mais attributs de droite du tuple
        for j in range(len(sigma)):  # On recupere les dependances excepte celle qu'on analyse
            if sigma[i] != sigma[j]:
                other_df.append(sigma[j])
                other_df_lhs.append(sigma[j][1])
        for k in range(len(other_df)):
            # Pour chaque autre dependance, on parcourt n fois (n = nb dep) le tableau des dependance pour
            # recuperer tous les attributs possibles grace a la partie gauche du tuple analyse car en effet
            # l'ajout d'un attribut d'une dependance peut en debloquer d'autres...
            if analysed_df[1].split(" ") not in acquired_lhs_array:
                acquired_lhs_array.append(analysed_df[1].split(" "))
            for m in range(len(other_df)):
                can_add = True
                for lhs in other_df[m][1].split(" "):
                    if lhs not in acquired_rhs_array + acquired_lhs_array[0]:
                        can_add = False
                if can_add:
                    acquired_rhs_array.append(other_df[m][2])
            if analysed_df[2] in acquired_rhs_array and analysed_df not in lgcl_csq_array:
                lgcl_csq_array.append(analysed_df)
    return lgcl_csq_array


def getAttributesNotExistDependencies(cursor):
    """

    :param cursor: the cursor of the database
    :return: the list of dependencies for which at least one of the attributes doesn't exist
    in the associated table
    """
    uselessDependencies = []
    cursor.execute("SELECT * FROM FuncDep")
    to_analyse_array = cursor.fetchall()
    for dependence in to_analyse_array:
        tableAttributes = Util.getAttributes(cursor, dependence[0])
        dependence_attributes = dependence[1].split(" ")
        dependence_attributes.append(dependence[2])
        for element in (dependence_attributes):
            if element not in tableAttributes and dependence not in uselessDependencies:
                uselessDependencies.append(dependence)
    return uselessDependencies


def getTableNotExistDependencies(cursor):
    """

    :param cursor: the cursor of the database
    :param relation: relation: the relation for which we check if the attributes exists
    :return: the list of dependencies for which the table doesn't exist
    """
    uselessDependencies = []
    cursor.execute("SELECT * FROM FuncDep")
    to_analyse_array = cursor.fetchall()  # Contient les tables utilisees dans FuncDep
    table_list = Util.getTables(cursor)  # Contient les tables de la base de donnees
    for table_name in to_analyse_array:
        found = False
        for table in table_list:
            if table_name[0] == table[0]:
                found = True
        if not found and table_name not in uselessDependencies:
            uselessDependencies.append(table_name)
    return uselessDependencies