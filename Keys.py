# -*- encoding : utf-8 -*-

import Util
import FunctionalDependencies
import copy
import DbObject

def getSuperkeys(cursor,myDatabase,table):
    """

    :param cursor: the cursor of the database where it gets the superkeys
    :param myDatabase: the database where it gets the superkeys
    :return: a list containing the superkeys
    """
    superkeys = []
    partsOfAttributes = Util.getPartsOf(Util.getAttributes(cursor,table))
    for subassembly in partsOfAttributes:
        if isSuperkey(cursor,myDatabase,subassembly,table):
            superkeys.append(subassembly)
    return superkeys


def isAllInList(list1, list2):
    """

    :return: True if all the elements of the list 1 are in the list 2
    """
    for elem in list1:
        if elem not in list2:
            return False
    return True


def isSuperkey(cursor,myDatabase,attributes,table):
    """

    :param cursor: the cursor of the database where it gets the superkeys
    :param myDatabase: the databasse where it gets the superkeys
    :param attributes: the attribute to check if it's a superkey
    :return: True if the attributes is a superkey
    """

    imgAllAttributes = copy.copy(Util.getAttributes(cursor, table))
    hadAttributes = copy.copy(attributes)
    funcDep = FunctionalDependencies.getFuncDep(myDatabase, cursor)
    funcDepCopy = copy.copy(funcDep)
    for i in range(len(funcDepCopy)):
        for df in funcDepCopy:
            if df[0] == table and isAllInList(df[1].split(" "), hadAttributes):
                hadAttributes.append(df[2])
                funcDepCopy.remove(df)

    if set(hadAttributes) == set(imgAllAttributes):
        return True
    else:
        return False

def getKeys(cursor,myDatabase,table):
    """

    :param cursor: the cursor of the database where it gets the keys
    :param myDatabase: the database where it gets the keys
    :return: a liste containing the keys
    """
    keys = []
    superkeys = getSuperkeys(cursor,myDatabase,table)
    length = 10000000000000
    for item in superkeys:
        if len(item) < length:
            length = len(item)
    for item in superkeys:
        if len(item) == length:
            keys.append(item)
    return keys