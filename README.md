# README #

Pour utiliser l'application, il suffit simplement d'ouvrir un terminal où se trouvent tous les fichiers ".py", ensuite
il faut lancer le programme en tapant la ligne suivante : python Run.py . Après il faut entrer le chemin de la base de
données que l'on veut utiliser. S'il n'y a pas de soucis lors de l'ouverture, on arrive dans le programme proprement dit,
et là, taper help pour obtenir toutes les commandes disponibles, taper help+nomcommande pour avoir plus de détails sur
la commande spécifiée.

NB :
    * Quelques bases de données "test" on été laissées dans le programme
    * Le fichier .git est évidemment caché
    * Malheureusement nous avons remarqué que la parfois, la dernière table de la décomposition ne devant contenir que
    la clé contient également un autre attribut qui ne devrait pas être là...