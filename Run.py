# -*- encoding : utf-8 -*-

import cmd
import sqlite3
import os.path
import DbObject
import Util
import FunctionalDependencies
import re
import DbProperties
import Keys

def print_dependence(dep):
    """

    :param dep: the array with the dependencies
    :return: /
    """
    count=0
    for part in dep:
        print count,
        print "> Table :",
        print part[0],
        print " | ",
        print part[1],
        print " => ",
        print part[2]
        count+=1

def is_database_path_correct (path):
    """

    :param path: path to the database we want to connect
    :return: True if the path is corresponding to a file
    """
    while not os.path.isfile(path):
        if path == "exit" :
            return -1
        print "La base de donnee specifiee n'existe pas, veuillez entrer un chemin correct"
        path = raw_input(">>> ")
    return path


def is_file_correct(path):
    """

    :param path: path to the database we want to connect
    :return: True if the file given by the path is a sqlite database
    """
    while os.path.getsize(path) < 100:  # Le header d'une database SQLite fait 100 bytes
        print "Le fichier specifiee n'est pas une base de donnee, veuillez entrer un nom de fichier correct"
        path = raw_input(">>> ")
        if path == "exit" :
            return -1
    with open(path, 'rb') as file:
        header = file.read(16)
        if header != 'SQLite format 3\x00':  # 16 premiers bytes d'un fichier sql3 de base de donnee
            print "Le fichier specifiee n'est pas une base de donnee, veuillez entrer un nom de fichier correct"
            path = raw_input(">>> ")
            if path != 'exit' :
                is_file_correct(path)
            else :
                return -1
        else :
            return path

print "Bienvenue dans l'outil d'analyse de base de donnees, veuillez introduire le chemin vers la base de " \
      "donnee a utiliser :"
print "*** Introduisez exit une ou deux fois pour quitter le programme a tout moment***"
input = raw_input(">>> ")

path = is_database_path_correct(input)
if path != -1:
    new_path = is_file_correct(path)
    if new_path != -1:

        class Run(cmd.Cmd):
            try:
                MyDbObject = DbObject.DbObject(input)
            except sqlite3.Error:
                print "Impossible de se connecter a la base de donnee ", input
            intro = "Vous etes bien connecte a la base de donnee, introduisez help pour plus d'informations" \
                    " sur les commandes disponibles"

            def do_show_tables(self, line):
                """ Displays the name of the tables in the database """
                print "Les tables de la base de donnees sont les suivantes :"
                Util.showTables(self.MyDbObject.cursor)

            def do_show(self, table):
                """ show [table]
                Displays the given table of the database """
                try:
                    attributes = Util.getAttributes(self.MyDbObject.cursor, table)
                    print "Attributs de la table :",
                    count = 0
                    for attribute in attributes:
                        print attribute,
                        count +=1
                        if count < len(attributes):
                            print ",",
                    print "\n----------------------------------------------------------------"
                    Util.printTable(table,self.MyDbObject.cursor)
                except sqlite3.OperationalError:
                    print "Pas de table de nom :", table

            def do_add(self, line):
                """ add [table, lhs, rhs]re
                Adds the functional dependence given in the table FuncDep """
                array = line.split(",")
                if len(array) != 3:
                    print "Erreur : Les arguments doivent etre de la forme : table, lhs, rhs"
                elif (re.search("^ *[a-zA-Z0-9_]+[ ]*$", array[0]) is None
                      or re.search("^ *[a-zA-Z0-9_]+[ ]*$", array[2]) is None):
                    print "Erreur : La table et rhs sont des chaines de caracteres ne contenant qu'un seul mot"
                else:
                    array_1_no_spaces = array[1].split()
                    array_1_string = ' '.join(array_1_no_spaces)
                    try:
                        FunctionalDependencies.addFuncDep(self.MyDbObject.myDatabase, self.MyDbObject.cursor,
                                                          array[0].strip(),array_1_string,array[2].strip())
                    except sqlite3.OperationalError:
                        print "Pas de table de nom :", array[0]


            def do_remove(self, line):
                """ Remove a chosen functional dependence (from the displayed list) from the table FuncDep"""
                print "Choisissez parmi les dependences suivantes celles a supprimer (entrer le numero de la ligne):"
                print "----------------------------------------------------------------"
                Util.printTable("FuncDep",self.MyDbObject.cursor)
                print "----------------------------------------------------------------"
                line_number = raw_input(">>> ")
                try:
                    line_number = int(line_number)
                    funcDep = FunctionalDependencies.getFuncDep(self.MyDbObject.myDatabase, self.MyDbObject.cursor)
                    print line_number
                    if line_number >= 0 and line_number <= len(funcDep):
                        to_remove_dep = funcDep[line_number]
                        FunctionalDependencies.removeFuncDep(self.MyDbObject.myDatabase, self.MyDbObject.cursor,
                                                        to_remove_dep[0], to_remove_dep[1], to_remove_dep[2])
                    else :
                        print "L'entier donne n'est pas valide"
                except ValueError:
                    print("Veuillez entrer un entier")


            def do_modify(self,line):
                """ Modifies a functional dependence in the table FuncDep """
                print "Choisissez parmi les dependences suivantes celle a modifier (entrer le numero de la ligne):"
                print "----------------------------------------------------------------"
                Util.printTable("FuncDep",self.MyDbObject.cursor)
                print "----------------------------------------------------------------"
                line_number = raw_input(">>> ")
                try:
                    line_number = int(line_number)
                    funcDep = FunctionalDependencies.getFuncDep(self.MyDbObject.myDatabase, self.MyDbObject.cursor)
                    if line_number >= 0 and line_number <= len(funcDep):
                        print "Entrez les nouvelles valeurs sous la forme suivante : table, lhs, rhs : "
                        new_values = raw_input(">>> ")
                        array = new_values.split(",")
                        if len(array) != 3:
                            print "Erreur : Les arguments doivent etre de la forme : table, lhs, rhs"
                        elif (re.search("^ *[a-zA-Z0-9_]+[ ]*$", array[0]) is None
                          or re.search("^ *[a-zA-Z0-9_]+[ ]*$", array[2]) is None):
                            print "Erreur : La table et rhs sont des chaines de caracteres ne contenant qu'un seul mot "
                        else:
                            array_1_no_spaces = array[1].split()
                            array_1_string = ' '.join(array_1_no_spaces)
                            to_modify_dep = funcDep[line_number]
                            if (array[0].strip() == to_modify_dep[0] and array_1_string == to_modify_dep[1]
                                and array[2].strip() == to_modify_dep[2]):
                                print "Valeurs inchangees...."
                            else :
                                FunctionalDependencies.modifFuncDep(self.MyDbObject.myDatabase, self.MyDbObject.cursor,
                                                            to_modify_dep[0], to_modify_dep[1], to_modify_dep[2],
                                                            array[0].strip(),array_1_string,array[2].strip())
                                print "La dependence a bien ete ajoutee"
                    else :
                        print "L'entier donne n'est pas valide"
                except ValueError:
                    print("Priere de recommencer l'operation avec un entier")


            def do_wrong_dep(self, line):
                """ Displays the incorrect/unsatisfied dependencies """
                found = False
                for dep_table in  Util.getTables(self.MyDbObject.cursor):
                    if dep_table[0] != "FuncDep":
                        unsatisfied_dependencies = \
                             FunctionalDependencies.getIncorrectDependencies(self.MyDbObject.cursor,dep_table[0])
                        if len(unsatisfied_dependencies) != 0:
                            found = True
                            print "Liste des dependences non-verifiees :",
                            print "-----------------------------------------------------------"
                            for dependence in unsatisfied_dependencies:
                                    print dependence[0],
                                    print " => ",
                                    print dependence[1]
                if not found:
                    print "Toutes les dependences sont satisfaites"


            def do_lgcl_csq(self, line):
                """ Displays all the logical consequences """
                found = False
                for dep_table in Util.getTables(self.MyDbObject.cursor):
                    if dep_table[0] != "FuncDep":
                        lgcl_csq = \
                             FunctionalDependencies.getLogicalConsequences(self.MyDbObject.cursor,dep_table[0])
                        if len(lgcl_csq) != 0:
                            found = True
                            print "Liste des consequences logique de la table",
                            print dep_table[0]
                            print "-----------------------------------------------------------"
                            for dependence in lgcl_csq:
                                    print dependence[1],
                                    print " => ",
                                    print dependence[2]
                if not found:
                    print "Il n'y a aucune consequence logique"

            def do_remove_dep(self, line):
                """ Proposes different choices to remove wrong dependencies """
                want_to_quit = False
                while not want_to_quit:
                    attributes_array = FunctionalDependencies.getAttributesNotExistDependencies(self.MyDbObject.cursor)
                    table_array = FunctionalDependencies.getTableNotExistDependencies(self.MyDbObject.cursor)
                    lgcl_array = []
                    for dep_table in Util.getTables(self.MyDbObject.cursor):
                        if dep_table[0] != "FuncDep":
                            lgcl_csq = \
                                 FunctionalDependencies.getLogicalConsequences(self.MyDbObject.cursor,dep_table[0])
                            if len(lgcl_csq) != 0:
                                lgcl_array.extend(lgcl_csq)
                    incorrect_array = []
                    for dep_table in  Util.getTables(self.MyDbObject.cursor):
                        if dep_table[0] != "FuncDep":
                            unsatisfied_dependencies = \
                                 FunctionalDependencies.getIncorrectDependencies(self.MyDbObject.cursor,dep_table[0])
                            if len(unsatisfied_dependencies) != 0:
                                incorrect_array.extend(unsatisfied_dependencies)
                    if (len(attributes_array)== 0 and len(lgcl_array)==0 and len(incorrect_array)==0
                        and len(table_array)==0):
                        print "Toutes les tables sont correctes ! Il n'y a rien a faire"
                        break
                    else :
                        print "Entrez le numero correspondant a vos envies, si un choix vous parait manquant c'est " \
                              "qu'il n'y a aucun probleme a ce niveau, il n'est donc pas affiche"
                        if len(attributes_array) != 0:
                            print 1,
                            print "> Choisir de corriger les erreurs d'attributs inexistants"
                        if len(table_array) != 0:
                            print 2,
                            print "> Choisir de corriger les erreurs de tables inexistantes"
                        if len(lgcl_array) !=0:
                            print 3,
                            print "> Choisir de corriger les problemes de consequences logiques"
                        if len(incorrect_array) !=0:
                            print 4,
                            print "> Choisir de corriger les problemes de dependances non respectees"
                        choice = raw_input(">>> ")
                        if choice == "exit":
                            want_to_quit = True
                        else:
                            try:
                                choice_number = int(choice)
                                if choice_number < 1 or choice_number > 4:
                                    while choice_number < 1 or choice_number > 4:
                                        print "Veuillez saisir un nombre valide entre 1 et 4"
                                        choice = raw_input(">>> ")
                                        choice_number = int(choice)
                                elif choice_number ==1:
                                    if len(attributes_array) == 0:
                                        print "Aucun pobleme d'attribut n'est present, veuillez " \
                                              "selectionner correctement une action"
                                        break
                                    else :
                                        print "Voici la(les) dependence(s) posant probleme(s) c'est a dire " \
                                              "avec des attributs inexistants :"
                                        print_dependence(attributes_array)
                                        print "Laquelle voulez vous supprimer ?"
                                        choice = raw_input(">>> ")
                                        try:
                                            choice_number = int(choice)
                                            if choice_number < 0 or choice_number > len(attributes_array):
                                                while choice_number < 0 or choice_number > len(attributes_array):
                                                    print "Veuillez saisir un nombre valide entre 0 et ",
                                                    print len(attributes_array)-1
                                                    choice = raw_input(">>> ")
                                                    choice_number = int(choice)
                                            FunctionalDependencies.removeFuncDep(self.MyDbObject.myDatabase,
                                         self.MyDbObject.cursor,attributes_array[choice_number][0],
                                         attributes_array[choice_number][1],attributes_array[choice_number][2])
                                        except ValueError:
                                            print("Errreur : La valeur entree n'est pas un entier")
                                elif choice_number ==2:
                                    if len(table_array) == 0:
                                        print "Aucun pobleme de tables n'est present, veuillez " \
                                              "selectionner correctement une action"
                                        break
                                    else :
                                        print "Voici la(les) dependence(s) posant probleme(s) au niveau " \
                                              "des nom de tables qui n'existent pas :"
                                        print_dependence(table_array)
                                        print "Laquelle voulez vous supprimer ?"
                                        choice = raw_input(">>> ")
                                        try:
                                            choice_number = int(choice)
                                            if choice_number < 0 or choice_number > len(table_array):
                                                while choice_number < 0 or choice_number > len(table_array):
                                                    print "Veuillez saisir un nombre valide entre 0 et ",
                                                    print len(table_array)-1
                                                    choice = raw_input(">>> ")
                                                    choice_number = int(choice)
                                            FunctionalDependencies.removeFuncDep(self.MyDbObject.myDatabase,
                                         self.MyDbObject.cursor,table_array[choice_number][0],
                                         table_array[choice_number][1],table_array[choice_number][2])
                                        except ValueError:
                                            print("Errreur : La valeur entree n'est pas un entier")
                                elif choice_number ==3:
                                    if len(lgcl_array) == 0:
                                        print "Aucune consequence logique n'a ete detectee, veuillez " \
                                              "selectionner correctement une action"
                                        break
                                    else :
                                        print "Voici la(les) consequence(s) logique(s) detectees :"
                                        print_dependence(lgcl_array)
                                        print "Laquelle voulez vous supprimer ?"
                                        choice = raw_input(">>> ")
                                        try:
                                            choice_number = int(choice)
                                            if choice_number < 0 or choice_number > len(lgcl_array):
                                                while choice_number < 0 or choice_number > len(lgcl_array):
                                                    print "Veuillez saisir un nombre valide entre 0 et ",
                                                    print len(lgcl_array)-1
                                                    choice = raw_input(">>> ")
                                                    choice_number = int(choice)
                                            FunctionalDependencies.removeFuncDep(self.MyDbObject.myDatabase,
                                         self.MyDbObject.cursor,lgcl_array[choice_number][0],
                                         lgcl_array[choice_number][1],lgcl_array[choice_number][2])
                                        except ValueError:
                                            print("Errreur : La valeur entree n'est pas un entier")
                                elif choice_number ==4:
                                    if len(incorrect_array) == 0:
                                        print "Aucune dependence non verifiee n'a ete detectee, veuillez " \
                                              "selectionner correctement une action"
                                        break
                                    else :
                                        print "Voici la(les) dependence(s) non verifiee(s)), :"
                                        print_dependence(incorrect_array)
                                        print "Laquelle voulez vous supprimer ?"
                                        choice = raw_input(">>> ")
                                        try:
                                            choice_number = int(choice)
                                            if choice_number < 0 or choice_number > len(incorrect_array):
                                                while choice_number < 0 or choice_number > len(incorrect_array):
                                                    print "Veuillez saisir un nombre valide entre 0 et ",
                                                    print len(incorrect_array)-1
                                                    choice = raw_input(">>> ")
                                                    choice_number = int(choice)
                                            FunctionalDependencies.removeFuncDep(self.MyDbObject.myDatabase,
                                         self.MyDbObject.cursor,incorrect_array[choice_number][0],
                                         incorrect_array[choice_number][1],incorrect_array[choice_number][2])
                                        except ValueError:
                                            print("Errreur : La valeur entree n'est pas un entier")
                            except ValueError:
                                print("Erreur : La valeur entree n'est pas un entier")

            def do_get_superkey(self, table):
                """ get_superkey [table]
                Print the superkeys of the table"""
                try:
                    superkeys = Keys.getSuperkeys(self.MyDbObject.cursor,self.MyDbObject.myDatabase,table)
                    for superkey in superkeys:
                        for i in range(len(superkey)):
                            print superkey[i],
                            if i != len(superkey)-1:
                                print ",",
                        print ""
                except sqlite3.OperationalError:
                    print "Pas de table de nom :", table

            def do_get_key(self, table):
                """ get_key [table]
                Print the keys of the table"""
                try:
                    keys = Keys.getKeys(self.MyDbObject.cursor,self.MyDbObject.myDatabase,table)
                    for key in keys:
                        for i in range(len(key)):
                            print key[i],
                            if i != len(key)-1:
                                print ',',
                        print ""
                except sqlite3.OperationalError:
                    print "Pas de table de nom :", table

            def do_is_BCNF(self, table):
                """ is_BCNF [table]
                Tells if a relation is in BCNF or not"""
                try:
                    isInBCNF = DbProperties.isInBCNF(self.MyDbObject.myDatabase,self.MyDbObject.cursor,table)
                    if isInBCNF == True:
                        print "La relation "+table+" est en BCNF"
                    else:
                        print "La relation "+table+" n'est pas en BCNF"
                    print "----------------------------------------------------------------"
                except sqlite3.OperationalError:
                    print "Pas de table de nom :", table

            def do_is_3NF(self, table):
                """ is_3NF [table]
                Tells if a relation is in 3NF or not"""
                try:
                    isIn3NF = DbProperties.isIn3NF(self.MyDbObject.myDatabase,self.MyDbObject.cursor,table)
                    if isIn3NF == True:
                        print "La relation "+table+" est en 3NF"
                    else:
                        print "La relation "+table+" n'est pas en 3NF"
                    print "----------------------------------------------------------------"
                except sqlite3.OperationalError:
                    print "Pas de table de nom :", table

            def do_decomp(self, line):
                """ Decompose all the tables of the current database"""
                try:
                    DbProperties.decomposition(self.MyDbObject.myDatabase,
                                               self.MyDbObject.cursor,self.MyDbObject.db_path)
                except sqlite3.OperationalError:
                    print "La base de donnee est deja creee"
                DbProperties.decomposition(self.MyDbObject.myDatabase,self.MyDbObject.cursor,self.MyDbObject.db_path)
                print "La base de donne a ete decomposee et possede le nom : nombasededonnee_3NF"

            def do_exit(self, line):
                self.MyDbObject.closeDb()
                return True

        if __name__ == '__main__':
            Run().cmdloop()

print "Goodbye!"