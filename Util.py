import sqlite3
import DbObject

def showTables(cursor):
    """

    :param cursor: the cursor of the database
    :return: print the name of tables contained in the database
    """
    for table in getTables(cursor):
        print "*", table[0]


def printTable(table, cursor):
    """

    :param table: the table to show
    :param cursor: the cursor of the database
    :return: print the content of the table
    """
    cursor.execute("SELECT * FROM " + table)
    my_table = cursor.fetchall()
    count = 0
    for row in my_table:
        print count,
        print ")",
        for row_element in row:
            print row_element,
            print '|',
        print ""
        count +=1

def isInTable(tuple,table, cursor):
    """

    :param tuple: the tuple to look for in the table
    :param table: the table to look in
    :param cursor: the cursor of the database
    :return: True if the tuple is in teh table
    """
    cursor.execute("SELECT * FROM " + table)
    while True:
        row = cursor.fetchone()
        if row == None:
            break
        if(tuple == row):
            return True
    return False

def getTables(cursor):
    """

    :param cursor: the cursor of the database
    :return: return a list containing the name of each table in the database
    """
    cursor.execute("SELECT name FROM sqlite_master where type='table';")
    return cursor.fetchall()

def getAttributes(cursor,table):
    """

    :param cursor: the cursor of the database
    :param table: the table where the attributes are
    """
    attributes = []
    cursor.execute("PRAGMA table_info("+table+")")
    while True:
        row = cursor.fetchone()
        if row == None:
            break
        attributes.append(row[1])
    return attributes

def getAllAttributes(cursor):
    """

    :param cursor: the cursor of the database where it gets the tables
    :return: all attributes of the database
    """
    allAttributes = []
    for table in getTables(cursor):
        if table != "FuncDep":
            allAttributes += getAttributes(cursor,table)
    return allAttributes

# ce code vient de http://gilles.dubois10.free.fr/Bases/Ensembles/parties.html
# ll liste les parties d'un ensemble
def getPartsOf(list):
    """

    :param list: the list to get the parts of
    :return: a list containing all the parts of the list in parameter
    """
    parts = [[]]
    while list:
        a=list[0]
        list=list[1:]
        Q=[x+[a] for x in parts]+parts
        parts=Q
    return parts